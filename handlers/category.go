package handlers

import (
	"leboncoin/contracts"
	"leboncoin/pkg/errors"
	"net/http"

	"github.com/gin-gonic/gin"
)

// GetCategories handler function
// @Summary get all categories
// @Description get all adverts categories
// @ID api-get-categories
// @Tags Category
// @Accept  json
// @Produce  json
// @Success 200 {array} contracts.CategoryDTO
// @Failure 400 {object} errors.Error
// @Failure 500 {object} errors.Error
// @Router /categories [get]
func (h *HTTP) GetCategories(c *gin.Context) {
	var categories []contracts.CategoryDTO
	response, err := h.Repo.GetCategories()
	if err != nil {
		c.JSON(http.StatusInternalServerError, errors.InternalServerError("Create Advert", "%v", err))
		return
	}
	for _, category := range response {
		categories = append(categories, contracts.CategoryDTO{
			ID: category.ID, 
			CategoryName: category.CategoryName,
		})
	}
	c.JSON(http.StatusOK, categories)
}