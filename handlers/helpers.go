package handlers

import (
	"net/http"
	"strings"

	"github.com/agnivade/levenshtein"
	"github.com/gin-gonic/gin"
)

type CalculateMatchRequest struct {
	ModelName string `json:"model_name"`
}

// CalculateClosestMatch handler function
// @Summary calculate closest match
// @Description calculate closest match
// @ID api-calculate-closest-match
// @Tags Advert
// @Accept  json
// @Produce  json
// @Param data body CalculateMatchRequest true "claculate request data"
// @Success 200 {object} contracts.AdvertCreatedDTO
// @Failure 400 {object} errors.Error
// @Failure 500 {object} errors.Error
// @Router /match [post]
func (h *HTTP) CalculateClosestMatch(c *gin.Context) {
	var request CalculateMatchRequest
	c.BindJSON(&request)
	m, d := h.GetClosestMatch(request.ModelName)
	c.JSON(http.StatusOK, gin.H{"match": m, "distance": d})
}

//CheckIfCarAdvert will check if the advert is a car advertise
//and return the closest match of the car brand and model
//using levenshtein algorithm
func (h *HTTP) GetClosestMatch(givenModelName string) (*string, *int) {
	var min int
	var minValue string
	models, err := h.Repo.GetAllModels()
	if err != nil {
		return nil, nil
	}
	min = levenshtein.ComputeDistance(strings.ToLower(models[0].ModelLabel), strings.ToLower(givenModelName))
	minValue = models[0].ModelLabel
	for _, model := range models {
		distance := levenshtein.ComputeDistance(strings.ToLower(model.ModelLabel), strings.ToLower(givenModelName))
		if distance < min {
			min = distance
			minValue = model.ModelLabel
		}
	}
	return &minValue, &min
}

func Min(array []int) int {
	var min = array[0]
    for _, value := range array {
        if min > value {
            min = value
        }
    }
    return min
}