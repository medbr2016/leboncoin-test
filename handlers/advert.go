package handlers

import (
	"leboncoin/contracts"
	"leboncoin/models"
	"leboncoin/pkg/errors"
	"net/http"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
)

type CreateAdvertRequest struct {
	Title       string `json:"title"`
	Description string `json:"description"`
	CategoryID  uint   `json:"category_id"`
	Model       string `json:"model"`
}

type UpdateAdvertRequest struct {
	Title       string `json:"title"`
	Description string `json:"description"`
}

// CreateAdvert handler function
// @Summary create advert
// @Description create a new advert
// @ID api-create-new-advert
// @Tags Advert
// @Accept  json
// @Produce  json
// @Param data body CreateAdvertRequest true "Advert request data"
// @Success 200 {object} contracts.AdvertCreatedDTO
// @Failure 400 {object} errors.Error
// @Failure 500 {object} errors.Error
// @Router /advert [post]
func (h *HTTP) CreateAdvert(c *gin.Context) {
	var request *CreateAdvertRequest
	var brandName, modelName string
	if err := c.BindJSON(&request); err != nil {
		c.JSON(http.StatusBadRequest, errors.BadRequest("Create Advert", "%v", err))
		return
	}
	advert := &models.Advert{
		Title:       request.Title,
		Description: request.Description,
		CategoryID:  request.CategoryID,
	}
	isCar, _ := h.Repo.IsCarAdvert(request.CategoryID)
	if isCar {
		match, _ := h.GetClosestMatch(request.Model)
		brand, model,  err := h.Repo.GetBrandByModelName(*match)
		if err != nil {
			c.JSON(http.StatusInternalServerError, errors.InternalServerError("Create Advert", "%v", err))
			return
		}
		if strings.EqualFold(*match, "") {
			c.JSON(http.StatusInternalServerError, errors.InternalServerError("Create Advert", "%v", "We didn't find the typed model in aour database"))
			return
		}
		advert.BrandID = &brand.ID
		advert.ModelID = &model.ID
		brandName = brand.BrandName
		modelName = model.ModelLabel
	}
	
	response, err := h.Repo.CreateAdvert(advert)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errors.InternalServerError("Create Advert", "%v", err))
		return
	}
	c.JSON(http.StatusCreated, contracts.AdvertCreatedDTO{
		ID: response.ID, 
		BrandName: brandName, 
		ModelName: modelName,
	})
}

// GetAdvert handler function
// @Summary get advert
// @Description get advert infos
// @ID api-get-advert
// @Tags Advert
// @Accept  json
// @Produce  json
// @Param id path int true "Advert id"
// @Success 200 {object} contracts.AdvertDTO
// @Failure 400 {object} errors.Error
// @Failure 500 {object} errors.Error
// @Router /advert/{id} [get]
func (h *HTTP) GetAdvert(c *gin.Context) {
	id := c.Param("id")
	advertId, err := strconv.ParseUint(id, 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, errors.BadRequest("Get Advert", "%v", err))
		return
	}
	advert, err := h.Repo.GetAdvert(uint(advertId))
	if err != nil {
		c.JSON(http.StatusInternalServerError, errors.InternalServerError("Get Advert", "%v", err))
		return
	}
	brand, err := h.Repo.GetBrandByID(*advert.BrandID)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errors.InternalServerError("Get Advert", "%v", err))
		return
	}
	model, err := h.Repo.GetModelByID(*advert.ModelID)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errors.InternalServerError("Get Advert", "%v", err))
		return
	}
	c.JSON(http.StatusOK, contracts.AdvertDTO{
		ID: advert.ID, 
		Title: advert.Title, 
		Description: advert.Description,
		BrandName: brand.BrandName,
		ModelName: model.ModelLabel,
	})
}

// UpdateAdvert handler function
// @Summary update advert
// @Description update advert infos
// @ID api-update-advert
// @Tags Advert
// @Accept  json
// @Produce  json
// @Param id path int true "Advert id"
// @Param id body UpdateAdvertRequest true "Advert data"
// @Success 200 {object} contracts.AdvertDTO
// @Failure 400 {object} errors.Error
// @Failure 500 {object} errors.Error
// @Router /advert/{id} [put]
func (h *HTTP) UpdateAdvert(c *gin.Context) {
	var advert UpdateAdvertRequest
	id := c.Param("id")
	advertId, err := strconv.ParseUint(id, 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, errors.BadRequest("Update Advert", "%v", err))
		return
	}
	if err = c.BindJSON(&advert); err != nil {
		c.JSON(http.StatusBadRequest, errors.BadRequest("Update Advert", "%v", err))
		return
	}
	ua := models.Advert{Title: advert.Title, Description: advert.Description}
	ua.ID = uint(advertId)
	updatedAdvert, err := h.Repo.UpdateAdvert(ua)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errors.InternalServerError("Update Advert", "%v", err))
		return
	}
	c.JSON(http.StatusOK, contracts.AdvertDTO{ID: updatedAdvert.ID, Title: updatedAdvert.Title, Description: updatedAdvert.Description})
}

// DeleteAdvert handler function
// @Summary delete advert
// @Description delete advert record
// @ID api-delete-advert
// @Tags Advert
// @Accept  json
// @Produce  json
// @Param id path int true "Advert id"
// @Success 200 {object} contracts.AdvertDTO
// @Failure 400 {object} errors.Error
// @Failure 500 {object} errors.Error
// @Router /advert/{id} [delete]
func (h *HTTP) DeleteAdvert(c *gin.Context) {
	id := c.Param("id")
	advertId, err := strconv.ParseUint(id, 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, errors.BadRequest("Delete Advert", "%v", err))
		return
	}
	if err := h.Repo.DeleteAdvert(uint(advertId)); err != nil {
		c.JSON(http.StatusInternalServerError, errors.InternalServerError("Delete Advert", "%v", err))
		return
	}
	c.JSON(http.StatusOK, gin.H{"status": "deleted"})
}