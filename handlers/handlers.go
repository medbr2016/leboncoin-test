package handlers

import (
	"net/http"
	"leboncoin/repository"
	"github.com/gin-gonic/gin"
)

type HTTP struct {
	Repo repository.Repository
}

func (h *HTTP) Health(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"health": "OK"})
}
