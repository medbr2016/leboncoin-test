FROM golang:1.17 AS build
WORKDIR /src
ADD . .
RUN go mod tidy
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o leboncointest .

FROM alpine
WORKDIR /app
COPY --from=build /src/docs /app/docs
COPY --from=build /src/leboncointest /app/
ENTRYPOINT /app/leboncointest
