package utils

import (
	"os"
	"strings"
)

func GetServerAddr() string {
	if strings.EqualFold(os.Getenv("SERVER_LISTEN_ADDR"), "") {
		return ":8080"
	}
	return os.Getenv("SERVER_LISTEN_ADDR")
}