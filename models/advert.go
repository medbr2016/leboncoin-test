package models

import (
	"fmt"

	"gorm.io/gorm"
)

type Advert struct {
	gorm.Model
	Title       string   `gorm:"not null;" json:"title"`
	Description string   `json:"description"`
	CategoryID  uint     `json:"category_id"`
	Category    Category `json:"category"`
	BrandID     *uint     `json:"brand_id,omitempty"`
	ModelID     *uint     `json:"model_id,omitempty"`
}

type Category struct {
	gorm.Model
	CategoryName string  `gorm:"not null;unique" json:"category_name"`
	Brands       []Brand `json:"brands,omitempty"`
}

type Brand struct {
	gorm.Model
	BrandName  string  `gorm:"not null;unique" json:"brand_name"`
	CategoryID uint    `json:"category_id"`
	Models     []Model `json:"models,omitempty"`
}

type Model struct {
	gorm.Model
	ModelLabel string `gorm:"not null;unique" json:"model_label"`
	BrandID    uint   `json:"brand_id"`
}

func SeedCategories(db *gorm.DB) {
	fmt.Println("Seeding fixtures data ...")
	db.FirstOrCreate(&Category{CategoryName: "Emploi"}, Category{CategoryName: "Emploi"})
	db.FirstOrCreate(&Category{CategoryName: "Immobilier"}, Category{CategoryName: "Immobilier"})
	db.FirstOrCreate(&Category{
		CategoryName: "Automobile",
		Brands: []Brand{
			{BrandName: "Audi", Models: GetModels("Audi")},
			{BrandName: "BMW", Models: GetModels("BMW")},
			{BrandName: "Citroen", Models: GetModels("Citroen")},
		}}, Category{CategoryName: "Automobile"})
}

func GetModels(brandName string) []Model {
	models := make(map[string][]string)
	models["Audi"] = []string{"Cabriolet", "Q2", "Q3", "Q5", "Q7", "Q8", "R8", "Rs3", "Rs4", "Rs5", "Rs7", "S3", "S4", "S4 Avant", "S4 Cabriolet", "S5", "S7", "S8", "SQ5", "SQ7", "Tt", "Tts", "V8"}
	models["BMW"] = []string{"M3", "M4", "M5", "M535", "M6", "M635", "Serie 1", "Serie 2", "Serie 3", "Serie 4", "Serie 5", "Serie 6", "Serie 7", "Serie 8"}
	models["Citroen"] = []string{"C1", "C15", "C2", "C25", "C25D", "C25E", "C25TD", "C3", "C3Aircross", "C3 Picasso", "C4", "C4 Picasso", "C5", "C6", "C8", "Ds3", "Ds4", "Ds5"}

	var modelsRecords []Model
	for _, v := range models[brandName] {
		modelsRecords = append(modelsRecords, Model{ModelLabel: v})
	}
	return modelsRecords
}
