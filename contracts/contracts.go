package contracts

type AdvertCreatedDTO struct {
	ID        uint   `json:"id"`
	BrandName string `json:"brand_name"`
	ModelName string `json:"model_name"`
}

type AdvertDTO struct {
	ID          uint   `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	BrandName   string `json:"brand_name"`
	ModelName   string `json:"model_name"`
}

type CategoryDTO struct {
	ID           uint   `json:"id"`
	CategoryName string `json:"category_name"`
}
