package repository

import (
	"leboncoin/models"

	"gorm.io/gorm"
)

type Repository interface {
	CreateAdvert(*models.Advert) (*models.Advert, error)
	GetAdvert(id uint) (*models.Advert, error)
	UpdateAdvert(advert models.Advert) (*models.Advert, error)
	DeleteAdvert(id uint) error
	GetCategories() ([]models.Category, error)
	GetAllModels() ([]models.Model, error)
	IsCarAdvert(id uint) (bool, error)
	GetBrandByModelName(name string) (*models.Brand, *models.Model, error)
	GetBrandByID(brandId uint) (*models.Brand, error)
	GetModelByID(modelId uint) (*models.Model, error)
}

type ApiRepo struct {
	Db *gorm.DB
}

func (r *ApiRepo) CreateAdvert(advert *models.Advert) (*models.Advert, error) {
	if err := r.Db.Create(&advert).Error; err != nil {
		return nil, err
	}
	return advert, nil
}

func (r *ApiRepo) GetAdvert(id uint) (*models.Advert, error) {
	advert := &models.Advert{}
	advert.ID = id
	if err := r.Db.First(&advert).Error; err != nil {
		return nil, err
	}
	return advert, nil
}

func (r *ApiRepo) GetBrandByID(brandId uint) (*models.Brand, error) {
	brand := &models.Brand{}
	brand.ID = brandId
	if err := r.Db.First(&brand).Error; err != nil {
		return nil, err
	}
	return brand, nil
}

func (r *ApiRepo) GetModelByID(modelId uint) (*models.Model, error) {

	model := &models.Model{}
	model.ID = modelId
	if err := r.Db.First(&model).Error; err != nil {
		return nil, err
	}
	return model, nil
}

func (r *ApiRepo) UpdateAdvert(advert models.Advert) (*models.Advert, error){
	if err := r.Db.Updates(&advert).Error; err != nil {
		return nil, err
	}
	return &advert, nil
}

func (r *ApiRepo) DeleteAdvert(id uint) error {
	if err := r.Db.Delete(&models.Advert{Model:gorm.Model{ID: id}}).Error; err != nil {
		return err
	}
	return  nil
}

func (r *ApiRepo) GetCategories() ([]models.Category, error) {
	var categories []models.Category
	if err := r.Db.Find(&categories).Error; err != nil {
		return nil, err
	}
	return categories, nil
}

func (r *ApiRepo) GetAllModels() ([]models.Model, error) {
	var models []models.Model
	if err := r.Db.Find(&models).Error; err != nil {
		return nil, err
	}
	return models, nil
}

func (r *ApiRepo) IsCarAdvert(id uint) (bool, error) {
	category := models.Category{}
	category.ID = id
	if err := r.Db.First(&category).Error; err != nil {
		return false, err
	}
	if category.CategoryName == "Automobile" {
		return true, nil
	}
	return false, nil
}

func (r* ApiRepo) GetBrandByModelName(name string) (*models.Brand, *models.Model, error) {
	var model models.Model
	var brand models.Brand
	if err := r.Db.Where("model_label = ?", name).First(&model).Error; err != nil {
		return nil, nil, err
	}
	brand.ID = model.BrandID
	if err := r.Db.First(&brand).Error; err != nil {
		return nil, nil, err
	}
	return &brand, &model, nil
}