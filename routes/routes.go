package routes

import (
	"github.com/gin-gonic/gin"
	docs "leboncoin/docs"
	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"leboncoin/handlers"
)

type Routes struct {
	Handlers handlers.HTTP
}

//RegisterRoutes declares routes
func (r *Routes) RegisterRoutes(router *gin.Engine) {
	router.GET("/health", r.Handlers.Health)
	// @BasePath /api/v1
	docs.SwaggerInfo.BasePath = "/api/v1"
	v1 := router.Group("/api/v1")
	{
		v1.POST("/match", r.Handlers.CalculateClosestMatch)
		v1Advert := v1.Group("/advert")
		{
			v1Advert.POST("", r.Handlers.CreateAdvert)
			v1Advert.GET("/:id", r.Handlers.GetAdvert)
			v1Advert.PUT("/:id", r.Handlers.UpdateAdvert)
			v1Advert.DELETE("/:id", r.Handlers.DeleteAdvert)
		}
		v1Categories := v1.Group("/categories")
		{
			v1Categories.GET("", r.Handlers.GetCategories)
		}
		//serve swagger docs
		v1.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))
	}

	//exemple of a new router group
	v2 := router.Group("/api/v2")
	{
		v2.GET("")
	}

}
