##The App
```bash
├── contracts           #the api service contracts folder
│   └── contracts.go
├── docker-compose.yml  #will creates 3 services : postgres, api, and adminer
├── Dockerfile
├── docs                #swagger specifiction generated automatically using annotations
│   ├── docs.go
│   ├── swagger.json
│   └── swagger.yaml
├── go.mod
├── go.sum
├── handlers            #our controllers 
│   ├── advert.go
│   ├── category.go
│   ├── handlers.go
│   └── helpers.go
├── main.go             #api entrypoint
├── models              #models folder
│   └── advert.go
├── pkg                 #include some helper packages
│   ├── database
│   │   └── db.go
│   ├── errors
│   │   └── errors.go
│   ├── middlewares
│   │   └── cors.go
│   └── utils
│       └── utils.go
├── Readme.md
├── repository         #repository implementation
│   └── repository.go
└── routes             #api routes 
    └── routes.go
```
##Build the app
you can build the api docker image and use docker-compose to create a postgres database and an adminer service to visualize data

```
docker-compose build
docker-compose up -d 
```

##Requests

I created a swagger documentation you can visit this url to see all endpoints ```http://localhost:8080/api/v1/swagger/index.html```
you will find the curls requests and all descriptions