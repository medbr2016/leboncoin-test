package main

import (
	"leboncoin/handlers"
	"leboncoin/models"
	"leboncoin/pkg/database"
	"leboncoin/pkg/middlewares"
	"leboncoin/pkg/utils"
	"leboncoin/repository"
	"leboncoin/routes"

	"github.com/gin-gonic/gin"
)

// @title Leboncoin test challenge API
// @version 1.0
// @description This is a swagger specification for the api.
func main() {
	router := gin.Default()
	router.Use(middlewares.CorsMiddleware())

	//init db connexion and migrate the database
	db := database.InitDatabase()
	db.AutoMigrate(&models.Advert{}, &models.Category{}, &models.Brand{}, &models.Model{})

	//load fixtures in the data base
	models.SeedCategories(db)

	// init the service and inject dependencies
	repo := &repository.ApiRepo{Db: db}
	r := routes.Routes{Handlers: handlers.HTTP{Repo: repo}}
	r.RegisterRoutes(router)

	//run the http server
	router.Run(utils.GetServerAddr())
}
